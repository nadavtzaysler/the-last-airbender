import logging
import os
import re
from typing import AnyStr, Self

from tlab.exceptions import InvalidPowerTypeError, InvalidPowerValueError

_ROCK_ATTACK_PATTERN: re.Pattern[AnyStr] = re.compile(
    r"rock ball",
    re.IGNORECASE,
)

class EarthBender:
    def __init__(
        self: Self,
        name: str,
        power: int,
    ) -> None:
        if (type(power) != int):
            raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._name = name
            self._power = int(power)
            self._skill = "Earthbending"

    @property
    def name(
        self: Self,
    ) -> str:
        return self._name

    @name.setter
    def name(
        self: Self,
        value: str,
    ) -> None:
        self._name = value

    @property
    def power(
        self: Self,
    ) -> int:
        return self._power

    @power.setter
    def power(
        self: Self,
        power: int,
    ) -> None:
        if (type(power) != int):
                raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._power = power

    @property
    def skill(
        self: Self,
    ) -> str:
        return self._skill
    
    @skill.setter
    def skill(
        self: Self,
        skill: str,
    ) -> None:
        self._skill = skill


    def bend(
        self: Self,
    ) -> None:
        earth_attack = os.getenv("EARTH_ATTACK")

        if _ROCK_ATTACK_PATTERN.match(earth_attack):
            print(f"rock ball with power: {self._power:^2}.")
        else:
            os.environ["EARTH_ATTACK"] = "No Rock Ball :("
            print("No Rock Ball :(")
