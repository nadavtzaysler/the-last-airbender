import logging
import random
from typing import Self

from tlab.exceptions import InvalidPowerTypeError, InvalidPowerValueError


class FireBender:
    def __init__(
        self: Self,
        name: str,
        power: int,
        random_generator: random.Random = random,
    ) -> None:
        if (type(power) != int):
            raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._name = name
            self._power = int(power)
            self._skill = "Firebending"
            self._random = random_generator

    @property
    def name(
        self: Self,
    ) -> str:
        return self._name

    @name.setter
    def name(
        self: Self,
        value: str,
    ) -> None:
        self._name = value

    @property
    def power(
        self: Self,
    ) -> int:
        return self._power

    @power.setter
    def power(
        self: Self,
        power: int,
    ) -> None:
        if (type(power) != int):
                raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._power = power

    @property
    def skill(
        self: Self,
    ) -> str:
        return self._skill
    
    @skill.setter
    def skill(
        self: Self,
        skill: str,
    ) -> None:
        self._skill = skill


    def bend(
        self: Self,
    ) -> None:
        rand =  self._random.randint(0, 6)
        if rand == 6:
            raise SystemExit(6)
        elif rand == 0:
            self.name = "dead"
        else:
            pass