import logging
from typing import Self

from tlab.exceptions import InvalidPowerTypeError, InvalidPowerValueError

class AirBender:
    def __init__(
        self: Self,
        name: str,
        power: int,
    ) -> None:
        if (type(power) != int):
            raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._name = name
            self._power = int(power)
            self._skill = "Airbending"

    @property
    def name(
        self: Self,
    ) -> str:
        return self._name

    @name.setter
    def name(
        self: Self,
        value: str,
    ) -> None:
        self._name = value

    @property
    def power(
        self: Self,
    ) -> int:
        return self._power

    @power.setter
    def power(
        self: Self,
        power: int,
    ) -> None:
        if (type(power) != int):
                raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._power = power

    @property
    def skill(
        self: Self,
    ) -> str:
        return self._skill
    
    @skill.setter
    def skill(
        self: Self,
        skill: str,
    ) -> None:
        self._skill = skill


    def bend(
        self: Self,
    ) -> None:
         logging.info(f"{self._name} is using his airbending skill!")
