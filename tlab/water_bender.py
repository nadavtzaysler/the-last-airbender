import os
from typing import Self
from webbrowser import BaseBrowser

from tlab.exceptions import InvalidPowerTypeError, InvalidPowerValueError


class WaterBender:
    # Return type is None due to: https://peps.python.org/pep-0484/#the-meaning-of-annotations
    def __init__(
        self: Self,
        name: str,
        power: int,
        browser: BaseBrowser | None = None,
    ) -> None:
        if (type(power) != int):
            raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._name = name
            self._power = int(power)
            self._skill = "Waterbending"
            self._browser = browser
    @property
    def name(
        self: Self,
    ) -> str:
        return self._name

    @name.setter
    def name(
        self: Self,
        value: str,
    ) -> None:
        self._name = value

    @property
    def power(
        self: Self,
    ) -> int:
        return self._power

    @power.setter
    def power(
        self: Self,
        power: int,
    ) -> None:
        if (type(power) != int):
                raise InvalidPowerTypeError("Power level must be a positive integer")
        if power < 0:
            raise InvalidPowerValueError("Power level must be a positive integer")
        else:
            self._power = power

    @property
    def skill(
        self: Self,
    ) -> str:
        return self._skill
    
    @skill.setter
    def skill(
        self: Self,
        skill: str,
    ) -> None:
        self._skill = skill

    def bend(
        self: Self,
    ) -> None:
        if self._browser is None:
            if self._power > 0:
                self._power -= 1
            return

        moon_phase = os.getenv("MOON", "NEW")

        base_url = "https://youtu.be/" if moon_phase != "FULL" else "https://www.wikiwand.com/en/"
        video_id = "gk-aCL6eyGc" if moon_phase == "NEW" else "6" if moon_phase == "FULL" else "weZKm1kTrpc"
        query_params = f"?si=XX45XZzc3a8uCN0o&t={self._power}" if moon_phase =="NEW" else "" if moon_phase == "FULL" else "?si=_Unblsn5tPvzwfs7"
        final_url = f"{base_url}{video_id}{query_params}"

        self._browser.open_new(final_url)