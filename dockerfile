FROM python:3.11

WORKDIR /app

COPY ./my_env /app/venv

ENV PATH="/app/venv/bin:${PATH}"

COPY ..

RUN chmod +x /app/venv/scripts/activate
CMD ["/app/venv/scripts/activate"]